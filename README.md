# PHP Stock Portfolio Simualtor

##TODO

### Database
- [ ] Create the user table (username, password, balance, signUpDate, balance)
- [ ] Create the UsersStock (tickerSymbol, username, quantity, datePurchased, priceBought)
- [ ] Create the StockInformation (tickerSymbol, companyName, currencyType, value, timeStamp, lastCloseValue)
- [ ] Create test cases and classes for each table

### Authentication
- [ ] Create the register and signup page
    - [ ] Create a view for registration
    - [ ] Create a controller for registration
    - [ ] Create a view for signup
    - [ ] Create a controller for signup
- [ ] Ensure the validation of the user (unique username)
- [ ] Redirect if you are already loged in (to home page profile)

### HomePage
- [ ] Ensure you are authenticated
    #### User Information
    - [ ] Generate all of the information
    - [ ] Create the necessary view to display it 
    - [ ] Create the proper controllers to deal with the request
    
    #### Stock Table
    - [ ] Generate the information for all the stocks 
    - [ ] Create a table view for the stock
    - [ ] Create the necessary controllers to deal with the request
    - [ ] Add the sell button to every row
    
### Stock Info
- [ ] Ensure authentication of the user
- [ ] Add the header of general stock information (company name and stock name)
    
    ### Create Stock Information View
    - [ ] If user owns some, then display amount, total value, price purchased at, profit (%)
    - [ ] Not matter what display Current Price, peek price, bottom price...
    - [ ] in PHP create the div that the graph should be place in
    
    ### Graph 
    - [ ] This is to be done using javascript
- [ ] Add the buy and sell buttons and the proper 
    
### Buy And Sell
- [ ] Create the view
- [ ] Create the controller
- [ ] Validate in php
- [ ] Validate in JavaScript
- [ ] Modify the database
- [ ] Note: Ensure that they have enough money, keep in mind the $10 transaction fee (selling you don't need $10 in the wallet)
