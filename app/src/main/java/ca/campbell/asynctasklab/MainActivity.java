package ca.campbell.asynctasklab;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.sql.Time;

/**
 * proof of concept code for lab 10 using AsyncTask
 * they have not yet done net i/ so this is a simple
 * thread that they can work on.
 *
 * Similar to a timer, the background thread sleeps
 * @ each second the UI thread is notified
 *
 * @author pmcampbell
 * @version 2018-11-12
 */
public class MainActivity extends Activity {

    private final String TAG = "PISHPOSH";
    ThreadAsyncTask thread = null;
    Button toggleButton;
    EditText duration;
    EditText frequency;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toggleButton  = (Button)findViewById(R.id.toggleButton);
        duration = (EditText)findViewById(R.id.duration);
        frequency = (EditText)findViewById(R.id.frequency);
    }

    @Override
    public void onPause() {
        super.onPause();
        thread.cancel(true);
    }

    public void start(View v) {
        Log.d(TAG, "start button hit");

        if (thread != null)
            Log.d(TAG, thread.isCancelled() + "");

        if (thread == null || thread.isCancelled()) {
            toggleButton.setText("Restart");
        } else {
            thread.cancel(false);
        }

        int durationTime = 0;
        try {
            durationTime = Integer.parseInt(duration.getText().toString());
        } catch (Exception e) {

        }

        int frequencyTime = 0;

        try {
            frequencyTime = Integer.parseInt(frequency.getText().toString());
        } catch (Exception e) {

        }

        thread = new ThreadAsyncTask();
        thread.execute(durationTime, frequencyTime);
    } // onClick()

    public  class ThreadAsyncTask extends AsyncTask<Integer, Void, Void> {
        static final int SECONDS = 40;
        Button b1, b2;

        // runs on background thread
        protected Void doInBackground(Integer... time) {
            Log.d(TAG, "Swapping for "+SECONDS);
            int i = 0;
            int duration = (time[0] != 0) ? time[0] : SECONDS;
            int frequency = (time[1] != 0) ? time[1] : 1;
            int numOfLoops = (int)(duration / frequency);
            for (i = 0; i < numOfLoops; i++) {

                //If the task has been cancelled, I need to stop it from running in the background here
                if (isCancelled())
                    break;

                // pause for the frequency time
                SystemClock.sleep(frequency * 1000);
                publishProgress();
            }
            return null;
        }
        protected void onProgressUpdate(Void... voids) {
            Log.d(TAG, "progress update");
            swapColors();
        }
        protected void swapColors() {
            Button b1 = (Button) findViewById(R.id.b1);
            Button b2 = (Button) findViewById(R.id.b2);
            if (b1.getText().toString()  == getResources().getString(R.string.pish) ) {
                b1.setText(R.string.posh);
                b1.setBackgroundColor(getResources().getColor(R.color.poshColour));
                b2.setText(R.string.pish);
                b2.setBackgroundColor(getResources().getColor(R.color.pishColour));
            } else {
                b2.setText(R.string.posh);
                b2.setBackgroundColor(getResources().getColor(R.color.poshColour));
                b1.setText(R.string.pish);
                b1.setBackgroundColor(getResources().getColor(R.color.pishColour));
            }
        }
        @Override
        protected void onCancelled() {
            Log.d(TAG, "BG Thread Canceled");
        }
        protected void onPreExecute(Void voids) {
            Log.d(TAG, "pre execute");
        }

        protected void onPostExecute(Void voids) {
            super.onPostExecute(voids);
            ((TextView)findViewById(R.id.tv)).setText("Finished");
            toggleButton.setText("Start");
        }
    } // AsyncTask class
}